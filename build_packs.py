#!/usr/bin/env python
#
import argparse
import datetime
import json
import os
import os.path
import sys
import subprocess

def _get_root_dir(root_dir=None):
    if root_dir is None:
        root_dir = os.getcwdu()
    return root_dir

def _get_server_dir(name, server_dir=None, root_dir=None):
    if server_dir is None:
        server_dir = os.path.join(root_dir, "server", name)
    return server_dir

def _get_web_dir(web_dir=None, root_dir=None):
    if web_dir is None:
        web_dir = os.path.join(root_dir, "web")
    return web_dir

def _get_java_bin(java_bin=None):
    if java_bin is None:
        java_home = os.getenv("JAVA_HOME")
        if java_home is None:
            java_bin = "java"
        else:
            java_bin = os.path.join(java_home, "bin", "java")
    return java_bin


def run_builder(args, java_bin=None, builder_jar=None, cp=False):
    java_bin = _get_java_bin(java_bin)
    if builder_jar is None:
        builder_jar = os.getenv("BUILDER_JAR")
    arguments = [java_bin, "-cp" if cp else "-jar", builder_jar] + args
    return subprocess.call(arguments)

def get_modpacks(root_dir=None):
    root_dir = _get_root_dir(root_dir)
    modpacks = list()
    for filename in os.listdir(root_dir):
        if is_modpack(filename, root_dir):
            modpacks.append(filename)
    return modpacks

def is_modpack(name, root_dir=None):
    root_dir = _get_root_dir(root_dir)
    filepath = os.path.join(root_dir, name, "modpack.json")
    return os.path.isfile(filepath)

def build_client_modpack(name, version, root_dir=None, output_dir=None, java_bin=None, builder_jar=None):
    root_dir = _get_root_dir(root_dir)
    output_dir = _get_web_dir(output_dir, root_dir)
    if not is_modpack(name, root_dir):
        raise NameError("There is no modpack called {}".format(name))
    returned = run_builder([ "--version", version,
                             "--input", os.path.join(root_dir, name),
                             "--output", output_dir,
                             "--manifest-dest", os.path.join(output_dir, name + ".json")
                             ], java_bin, builder_jar)
    return (returned == 0)

def get_forge(name, root_dir=None):
    root_dir = _get_root_dir(root_dir)
    loaders_dir = os.path.join(root_dir, name, "loaders")
    if not os.path.isdir(loaders_dir):
        return None
    for filename in os.listdir(loaders_dir):
        if filename.startswith("forge-") and filename.endswith("-installer.jar"):
            return os.path.join(loaders_dir, filename)

def is_forge_installed(name, server_dir=None, root_dir=None):
    root_dir = _get_root_dir(root_dir)
    server_dir = _get_server_dir(name, server_dir, root_dir)
    if not os.path.isdir(server_dir):
        return False
    for filename in os.listdir(server_dir):
        if filename.startswith("forge-") and filename.endswith("-universal.jar"):
            return True
    return False

def build_server_modpack(name, root_dir=None, output_dir=None, java_bin=None, builder_jar=None):
    root_dir = _get_root_dir(root_dir)
    output_dir = _get_server_dir(name, output_dir, root_dir)
    if not is_modpack(name, root_dir):
        raise NameError("There is no modpack called {}".format(name))
    returned = run_builder([ "com.skcraft.launcher.builder.ServerCopyExport",
                             "--source" , os.path.join(root_dir, name, "src"),
                             "--dest", output_dir,
                             ], java_bin, builder_jar, cp=True)
    return (returned == 0)

def install_server_forge(name, root_dir=None, output_dir=None, java_bin=None):
    root_dir = _get_root_dir(root_dir)
    output_dir = _get_server_dir(name, output_dir, root_dir)
    if is_forge_installed(name, output_dir, root_dir):
        return None
    if not is_modpack(name, root_dir):
        raise NameError("There is no modpack called {}".format(name))
    try:
        os.makedirs(output_dir)
    except:
        pass
    forge_jar = get_forge(name, root_dir)
    java_bin = _get_java_bin(java_bin)
    old_cwd = os.getcwdu()
    os.chdir(output_dir)
    returned = subprocess.call([java_bin, "-jar", forge_jar, "--installServer"])
    os.chdir(old_cwd)
    return (returned == 0)

def generate_packages_file(root_dir=None, web_dir=None):
    root_dir = _get_root_dir(root_dir)
    web_dir = _get_web_dir(web_dir, root_dir)
    packages = list()
    for filename in os.listdir(web_dir):
        if filename.endswith(".json"):
            modpack_metafile_name = os.path.join(web_dir, filename)
            with open(modpack_metafile_name, "r") as modpack_metafile:
                modpack_metadata = json.load(modpack_metafile)
                title = modpack_metadata.get("title")
                name = modpack_metadata.get("name")
                version = modpack_metadata.get("version")
                if title and name and version:
                    package = { "name": name,
                                "title": title,
                                "version": version,
                                "location": filename,
                                "priority": 1,
                              }
                    packages.append(package)
    packages_metadata = { "minimumVersion": 1, "packages": packages }
    with open(os.path.join(web_dir, "packages.json"), "w") as packages_file:
        json.dump(packages_metadata, packages_file)



def build_modpack(name, version, root_dir=None, web_dir=None, server_dir=None, java_bin=None, builder_jar=None):
    build_client_modpack(name, version, root_dir, web_dir, java_bin, builder_jar)
    build_server_modpack(name, root_dir, server_dir, java_bin, builder_jar)
    install_server_forge(name, root_dir, server_dir, java_bin)
    generate_packages_file(root_dir, web_dir)

def main(args):
    parser = argparse.ArgumentParser(prog=args[0], description='This script simplifies building of modpacks for SK Launcher.')
    parser.add_argument("-n", "--name", action="store", dest="name", help="Name of modpack which will be builded. If omitted, all modpacks will be built.")
    parser.add_argument("-v", "--version", action="store", dest="version", help="Version of modpack, If omitted, version will be date in 0.0.0.YYMMDDhhmmss format.")
    parser.add_argument("-d", "--root", action="store", dest="root", help="Root directory with all modpacks. Default is current working directory")
    parser.add_argument("-s", "--server_dir", action="store", dest="server_dir", help="Directory with server files. By default it is ROOT_DIR/server/MODPACK_NAME")
    parser.add_argument("-w", "--web_dir", action="store", dest="web_dir", help="Directory with webserver files. By default is is ROOT_DIR/web")
    parser.add_argument("-j", "--java", action="store", dest="java_bin", help="Path to java binary. by default it uses either one in PATH or JAVA_HOME")
    parser.add_argument("-b", "--builder", action="store", dest="builder_bin", help="Path to builder jar. By default it uses one in BUILDER_JAR environment variable.")
    options = parser.parse_args(args[1:])

    modpacks_for_building = []
    version = datetime.datetime.utcnow().strftime("0.0.0.%y%m%d%H%M%S")

    if options.name:
        modpacks_for_building.append(options.name)
    else:
        modpacks_for_building = get_modpacks(options.root)
    if options.version:
        version = options.version
    for modpack in modpacks_for_building:
        build_modpack(modpack, version, root_dir=options.root, web_dir=options.web_dir, server_dir=options.server_dir, java_bin=options.java_bin, builder_jar=options.builder_bin)

if __name__ == "__main__":
    main(sys.argv)

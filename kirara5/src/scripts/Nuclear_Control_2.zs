// --- Range Upgrade ---
recipes.addShaped(<IC2NuclearControl:ItemUpgrade>, [
[<IC2:reactorCoolantSimple:1>, <IC2:reactorCoolantSimple:1>, <IC2:reactorCoolantSimple:1>],
[<gregtech:gt.blockmachines:1366>, <IC2:itemFreq>, <gregtech:gt.blockmachines:1366>],
[null, null, null]]);

// --- Portable Information Panel ---
recipes.addShaped(<IC2NuclearControl:remoteMonitor>, [
[<gregtech:gt.blockmachines:1246>, null, null],
[<IC2:itemFreq>, <IC2NuclearControl:blockNuclearControlMain:5>, <IC2:itemFreq>],
[<IC2NuclearControl:ItemUpgrade>, <IC2:itemPartCarbonPlate>, <IC2:itemPartCarbonPlate>]]);

// --- Energy Counter ---
recipes.addShaped(<IC2NuclearControl:blockNuclearControlMain:6>, [
[<ore:plateIron>, <ore:circuitAdvanced>, <ore:plateIron>],
[<gregtech:gt.blockmachines:1426>, <IC2:blockElectric:4>, <gregtech:gt.blockmachines:1426>],
[null, null, null]]);

// --- Average Counter ---
recipes.addShaped(<IC2NuclearControl:blockNuclearControlMain:7>, [
[<ore:plateLead>, <ore:circuitAdvanced>, <ore:plateLead>],
[<gregtech:gt.blockmachines:1426>, <IC2:blockElectric:4>, <gregtech:gt.blockmachines:1426>],
[null, null, null]]);

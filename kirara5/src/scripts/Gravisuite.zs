// --- Created by Kosya
// --- Modified by OrdoFree
// --- Modified by Zulfurlubak for K5 (latest update 26.07.2017)

val iridiumPlate = <ore:plateAlloyIridium>;

val tsDrillHead = <ore:toolHeadDrillTungstenSteel>;

val tsChainsawHead = <ore:toolHeadChainsawTungstenSteel>;


// --- Advanced NanoChestPlate
recipes.remove(<GraviSuite:advNanoChestPlate:27>);

// --- Advanced Electric Jetpack
recipes.removeShaped(<GraviSuite:advJetpack:27>);

// --- Advanced Lappack
recipes.remove(<GraviSuite:advLappack:1>);

// --- Ultimate Lappack
recipes.remove(<GraviSuite:ultimateLappack:27>);

// --- Engine Booster
recipes.remove(<GraviSuite:itemSimpleItem:6>);

// --- Gravitation Engine
recipes.remove(<GraviSuite:itemSimpleItem:3>);

// --- Superconductor cover
recipes.remove(<GraviSuite:itemSimpleItem>);

// --- Magnetron
recipes.remove(<GraviSuite:itemSimpleItem:4>);

// --- Cooling Core
recipes.remove(<GraviSuite:itemSimpleItem:2>);

// --- Vajra Core
recipes.remove(<GraviSuite:itemSimpleItem:5>);

// --- Superconductor
recipes.removeShaped(<GraviSuite:itemSimpleItem:1>);

// --- Relocator
recipes.removeShaped(<GraviSuite:relocator:27>);

// --- Drill
recipes.remove(<GraviSuite:advDDrill>);

// --- Chainsaw
recipes.remove(<GraviSuite:advChainsaw>);


// --- Superconductor
recipes.addShaped(<GraviSuite:itemSimpleItem:1>, [
[<gregtech:gt.blockmachines:2020>, <gregtech:gt.blockmachines:2020>, <gregtech:gt.blockmachines:2020>],
[<gregtech:gt.blockmachines:2020>, iridiumPlate, <gregtech:gt.blockmachines:2020>],
[<gregtech:gt.blockmachines:2020>, <gregtech:gt.blockmachines:2020>, <gregtech:gt.blockmachines:2020>]]);


recipes.addShaped(<GraviSuite:advDDrill>, 
[[<gregtech:gt.metaitem.02:8374>,<gregtech:gt.metaitem.02:8374>, <gregtech:gt.metaitem.02:8374>],
[<IC2:upgradeModule>, <IC2:itemToolDDrill>, <IC2:upgradeModule>],
[<ore:circuitAdvanced>, <IC2:upgradeModule>, <ore:circuitAdvanced>]]);


recipes.addShaped(<GraviSuite:advChainsaw>, 
[[tsChainsawHead, tsChainsawHead, tsChainsawHead],
[<IC2:upgradeModule>, <IC2:itemToolChainsaw:1>, <IC2:upgradeModule>],
[<ore:circuitAdvanced>, <IC2:upgradeModule>, <ore:circuitAdvanced>]]);


// --- Cooling Core
recipes.addShaped(<GraviSuite:itemSimpleItem:2>, [
[<IC2:reactorVentDiamond:1>, <IC2:reactorHeatSwitchDiamond:1>, <IC2:reactorVentDiamond:1>],
[<gregtech:gt.180k_NaK_Coolantcell>, <ore:plateAlloyIridium>, <gregtech:gt.180k_NaK_Coolantcell>],
[<IC2:reactorPlatingHeat>, <IC2:reactorHeatSwitchDiamond:1>, <IC2:reactorPlatingHeat>]]);
// -
recipes.addShaped(<GraviSuite:itemSimpleItem:2>, [
[<IC2:reactorVentDiamond:1>, <IC2:reactorHeatSwitchDiamond:1>, <IC2:reactorVentDiamond:1>],
[<gregtech:gt.180k_Helium_Coolantcell>, <ore:plateAlloyIridium>, <gregtech:gt.180k_Helium_Coolantcell>],
[<IC2:reactorPlatingHeat>, <IC2:reactorHeatSwitchDiamond:1>, <IC2:reactorPlatingHeat>]]);

// --- Gravitation Engine
recipes.addShaped(<GraviSuite:itemSimpleItem:3>, [
[<IC2:blockMachine2:1>, <GraviSuite:itemSimpleItem:1>, <IC2:blockMachine2:1>],
[<GraviSuite:itemSimpleItem:2>, <gregtech:gt.blockmachines:23>, <GraviSuite:itemSimpleItem:2>],
[<IC2:blockMachine2:1>, <GraviSuite:itemSimpleItem:1>, <IC2:blockMachine2:1>]]);
 
// --- Magnetron
recipes.addShaped(<GraviSuite:itemSimpleItem:4>, [
[<ore:plateDenseNeodymiumMagnetic>, <IC2:itemRecipePart>, <ore:plateDenseNeodymiumMagnetic>],
[<ore:plateDenseCopper>, <GraviSuite:itemSimpleItem:1>, <ore:plateDenseCopper>],
[<ore:plateDenseNeodymiumMagnetic>, <IC2:itemRecipePart>, <ore:plateDenseNeodymiumMagnetic>]]);
 
// --- Vajra Core
recipes.addShaped(<GraviSuite:itemSimpleItem:5>, [
[<ore:craftingToolWrench>, <GraviSuite:itemSimpleItem:4>, <ore:craftingToolHardHammer>],
[<IC2:itemPartIridium>, <IC2:blockMachine2:1>, <IC2:itemPartIridium>],
[<GraviSuite:itemSimpleItem:1>, <gregtech:gt.blockmachines:23>, <GraviSuite:itemSimpleItem:1>]]);

// --- Vajra
recipes.addShaped(<GraviSuite:vajra:27>, [
[<gregtech:gt.metaitem.01:32673>, <GraviSuite:itemSimpleItem:4>, <ore:lensReinforcedGlass>],
[<ore:plateAlloyCarbon>, <GraviSuite:itemSimpleItem:5>, <ore:plateAlloyCarbon>],
[<ore:plateAlloyIridium>, <ore:batteryMaster>, <ore:plateAlloyIridium>]]);
 
// --- Engine Boost
recipes.addShaped(<GraviSuite:itemSimpleItem:6>, [
[<gregtech:gt.metaitem.01:20028>, <IC2:itemPartAlloy>, <gregtech:gt.metaitem.01:20028>],
[<ore:circuitAdvanced>, <IC2:upgradeModule>, <ore:circuitAdvanced>],
[<IC2:reactorPlating>, <IC2:reactorVentDiamond:1>, <IC2:reactorPlating>]]);

// --- Gravi Chest
//recipes.addShaped(<GraviSuite:graviChestPlate:27>, [
//[<GraviSuite:itemSimpleItem:1>, <IC2:itemArmorQuantumChestplate:*>, <GraviSuite:itemSimpleItem:1>],
//[<GraviSuite:itemSimpleItem:3>, <gregtech:gt.blockmachines:23>, <GraviSuite:itemSimpleItem:3>],
//[<GraviSuite:itemSimpleItem:1>, <GraviSuite:ultimateLappack:*>, <GraviSuite:itemSimpleItem:1>]]);
 
// --- Advanced NanoChestPlate
recipes.addShaped(<GraviSuite:advNanoChestPlate:27>, [
[<IC2:itemPartCarbonPlate>, <GraviSuite:advJetpack:*>, <IC2:itemPartCarbonPlate>],
[<ore:plateIridium>, <IC2:itemArmorNanoChestplate:*>, <ore:plateIridium>],
[<ore:wireGt12Platinum>, <ore:circuitData>, <ore:wireGt12Platinum>]]);
 
// --- Advanced Jetpack
recipes.addShaped(<GraviSuite:advJetpack:27>, [
[<gregtech:gt.metaitem.01:20372>, <IC2:itemArmorJetpackElectric:*>, <gregtech:gt.metaitem.01:20372>],
[<GraviSuite:itemSimpleItem:6>, <GraviSuite:advLappack:*>, <GraviSuite:itemSimpleItem:6>],
[<gregtech:gt.blockmachines:1643>, <gregtech:gt.metaitem.01:32706>, <gregtech:gt.blockmachines:1643>]]);

// --- Advanced Lappack
recipes.addShaped(<GraviSuite:advLappack:27>, [
[<gregtech:gt.metaitem.01:22313>, <ore:batteryElite>, <gregtech:gt.metaitem.01:22313>],
[<ore:batteryElite>, <IC2:itemArmorEnergypack:*>, <ore:batteryElite>],
[<ore:circuitData>, <ore:wireGt12Nichrome> , <ore:circuitData>]]);

// --- Ultimate Lappack
recipes.addShaped(<GraviSuite:ultimateLappack:27>, [
[<gregtech:gt.metaitem.01:22370>, <ore:batteryMaster>, <gregtech:gt.metaitem.01:22370>],
[<ore:batteryMaster>, <GraviSuite:advLappack:*>, <ore:batteryMaster>],
[<ore:circuitMaster>, <ore:wireGt16NiobiumTitanium>, <ore:circuitMaster>]]);

// --- Relocator
recipes.addShaped(<GraviSuite:relocator:27>, [
[<ore:plateAlloyIridium>, <ore:gemNetherStar>, <ore:plateNeutronium>],
[<ore:gemEnderEye>, <ore:craftingTeleporter>, <minecraft:dragon_egg>.reuse()],
[<ore:plateNaquadah>, <gregtech:gt.metaitem.01:32674>, <ore:plateOsmium>]]);


// --- Nei Change Names ---


// --- SuperConductorCover
<GraviSuite:itemSimpleItem>.displayName = "Superconductor Cover";

// --- SuperConductor
<GraviSuite:itemSimpleItem:1>.displayName = "Superconductor";

// --- Cooling Core
<GraviSuite:itemSimpleItem:2>.displayName = "Cooling Core";

// --- Gravitation Engine
<GraviSuite:itemSimpleItem:3>.displayName = "Gravitation Engine";

// --- Magnetron
<GraviSuite:itemSimpleItem:4>.displayName = "Magnetron";

// --- VajraCore
<GraviSuite:itemSimpleItem:5>.displayName = "Vajra Core";

// --- EngineBoost
<GraviSuite:itemSimpleItem:6>.displayName = "Engine Booster";
//created by keda
import mods.ic2.Macerator;
import mods.ic2.Compressor;
val Furnace = <minecraft:furnace>;


<ore:cobblestone>.add(<kirarautils:tile.cobble1:0>);
<ore:cobblestone>.add(<kirarautils:tile.cobble1:1>);
<ore:cobblestone>.add(<kirarautils:tile.cobble1:2>);
<ore:cobblestone>.add(<kirarautils:tile.cobble1:3>);
<ore:cobblestone>.add(<kirarautils:tile.cobble1:4>);
<ore:cobblestone>.add(<kirarautils:tile.cobble1:5>);
<ore:cobblestone>.add(<kirarautils:tile.cobble1:6>);
<ore:cobblestone>.add(<kirarautils:tile.cobble1:7>);
<ore:cobblestone>.add(<kirarautils:tile.cobble1:8>);
<ore:cobblestone>.add(<kirarautils:tile.cobble1:9>);

<ore:stoneCobble>.add(<kirarautils:tile.cobble1:0>);
<ore:stoneCobble>.add(<kirarautils:tile.cobble1:1>);
<ore:stoneCobble>.add(<kirarautils:tile.cobble1:2>);
<ore:stoneCobble>.add(<kirarautils:tile.cobble1:3>);
<ore:stoneCobble>.add(<kirarautils:tile.cobble1:4>);
<ore:stoneCobble>.add(<kirarautils:tile.cobble1:5>);
<ore:stoneCobble>.add(<kirarautils:tile.cobble1:6>);
<ore:stoneCobble>.add(<kirarautils:tile.cobble1:7>);
<ore:stoneCobble>.add(<kirarautils:tile.cobble1:8>);
<ore:stoneCobble>.add(<kirarautils:tile.cobble1:9>);
<ore:oreOilsands>.add(<PFAAGeologica:crudeSand>);
//<ore:oreIron>.remove(<SGU:BandedIron>);
//<ore:oreAnyIron>.remove(<SGU:BandedIron>);

Macerator.addRecipe(<kirarautils:tile.cobble1:0>, <SGU:AlumShale>);
Macerator.addRecipe(<kirarautils:tile.cobble1:1>, <SGU:Andesite>);
Macerator.addRecipe(<kirarautils:tile.cobble1:2>, <SGU:Diorite>);
Macerator.addRecipe(<kirarautils:tile.cobble1:3>, <SGU:Gabbro>);
Macerator.addRecipe(<kirarautils:tile.cobble1:4>, <SGU:Gneiss>);
Macerator.addRecipe(<kirarautils:tile.cobble1:5>, <SGU:Granite>);
//Macerator.addRecipe(<kirarautils:tile.cobble1:6>, <SGU:Kimberlite>);
Macerator.addRecipe(<kirarautils:tile.cobble1:7>, <SGU:Limestone>);
Macerator.addRecipe(<kirarautils:tile.cobble1:8>, <SGU:Marble>);
Macerator.addRecipe(<kirarautils:tile.cobble1:9>, <SGU:Shale>);


furnace.setFuel(<SGU:AlumShale>, 0);

furnace.addRecipe(<SGU:AlumShale>, <kirarautils:tile.cobble1:0>);
furnace.addRecipe(<SGU:Andesite>, <kirarautils:tile.cobble1:1>);
furnace.addRecipe(<SGU:Diorite>, <kirarautils:tile.cobble1:2>);
furnace.addRecipe(<SGU:Gabbro>, <kirarautils:tile.cobble1:3>);
furnace.addRecipe(<SGU:Gneiss>, <kirarautils:tile.cobble1:4>);
furnace.addRecipe(<SGU:Granite>, <kirarautils:tile.cobble1:5>);
//furnace.addRecipe(<SGU:Kimberlite>, <kirarautils:tile.cobble1:6>);
furnace.addRecipe(<SGU:Limestone>, <kirarautils:tile.cobble1:7>);
furnace.addRecipe(<SGU:Marble>, <kirarautils:tile.cobble1:8>);
furnace.addRecipe(<SGU:Shale>, <kirarautils:tile.cobble1:9>);
//Furnace.addRecipe(Iron * 2, tile.oreGold);

recipes.remove(<StorageDrawers:upgrade:6>);
recipes.remove(<StorageDrawers:upgrade:5>);
recipes.remove(<StorageDrawers:upgrade:4>);
recipes.remove(<StorageDrawers:upgrade:3>);
recipes.remove(<StorageDrawers:upgrade:2>);
recipes.addShaped(<StorageDrawers:upgrade:6>, 
	[[<ore:plateAlloyIridium>, <ore:plateAlloyIridium>, <ore:plateAlloyIridium>], 
	[<ore:plateAlloyIridium>, <ore:blockCoil>, <ore:plateAlloyIridium>], 
	[<ore:plateAlloyIridium>, <ore:circuitMaster>, <ore:plateAlloyIridium>]]);
recipes.addShaped(<StorageDrawers:upgrade:5>, 
	[[<ore:plateTungstenSteel>, <ore:plateTungstenSteel>, <ore:plateTungstenSteel>], 
	[<ore:plateTungstenSteel>, <ore:coilTitanium>, <ore:plateTungstenSteel>], 
	[<ore:plateTungstenSteel>, <ore:circuitData>, <ore:plateTungstenSteel>]]);
recipes.addShaped(<StorageDrawers:upgrade:4> * 2, 
	[[<ore:plateTitanium>, <ore:plateTitanium>, <ore:plateTitanium>], 
	[<ore:plateTitanium>, <libVulpes:libVulpescoil0:9>, <ore:plateTitanium>], 
	[<ore:plateTitanium>, <ore:circuitAdvanced>, <ore:plateTitanium>]]);
recipes.addShaped(<StorageDrawers:upgrade:3> * 2, 
	[[<ore:plateAluminium>, <ore:plateAluminium>, <ore:plateAluminium>], 
	[<ore:plateAluminium>, <ore:coilGold>, <ore:plateAluminium>], 
	[<ore:plateAluminium>, <ore:circuitGood>, <ore:plateAluminium>]]);
recipes.addShaped(<StorageDrawers:upgrade:2> * 3, 
	[[<ore:plateAnyIron>, <ore:plateAnyIron>, <ore:plateAnyIron>], 
	[<ore:plateAnyIron>, null, <ore:plateAnyIron>], 
	[<ore:plateAnyIron>, <ore:circuitBasic>, <ore:plateAnyIron>]]);
recipes.addShaped(<TConstruct:CraftedSoil:1> * 2, [[<minecraft:clay_ball>, <ore:sand>], [<ore:dustStone>, <minecraft:gravel>]]);
recipes.addShaped(<SGU:AlumShale>, [[<ore:stoneCobble>]]);
//stonedust
recipes.addShapeless(<IC2:itemDust:9>,[<ore:craftingToolMortar>,<ore:stoneCobble>]);
recipes.addShapeless(<IC2:itemDust:9>,[<ore:craftingToolMortar>,<ore:stone>]);
//grout
recipes.remove(<TConstruct:CraftedSoil:1>);
recipes.addShapeless(<TConstruct:CraftedSoil:1> * 2, [
	<ore:dustStone>, 
<minecraft:gravel>,
<minecraft:clay_ball>,
<ore:sand>]);


//harvestcraft, sinks
recipes.remove(<harvestcraft:sink:0>);
recipes.remove(<harvestcraft:sink:1>);
recipes.remove(<harvestcraft:sink:2>);
recipes.remove(<harvestcraft:sink:3>);

//Tinkers tool forge nerf
recipes.remove(<TConstruct:CraftingSlab:5>);

recipes.remove(<IC2:itemPartIndustrialDiamond>);

//Convert regular slimeballs to blue slimeballs and also blue slimeballs back to regular slimeballs
recipes.addShapeless(<TConstruct:strangeFood>, [<minecraft:slime_ball>]);
recipes.addShapeless(<minecraft:slime_ball>, [<TConstruct:strangeFood>]);


//recipes.remove(<TConstruct:knapsack>);
//nexey SGU sand
//recipes.addShaped(<minecraft:sandstone>, [[<SGU:SGUSand>, <SGU:SGUSand>], [<SGU:SGUSand>, <SGU:SGUSand>], [null]]);
//copper->redstone
recipes.removeShapeless(<minecraft:redstone>, [<gregtech:gt.metaitem.01:11035>]);


//magnetic craft tanks
recipes.remove(<Magneticraft:mg_tank> * 2);
recipes.addShaped(<Magneticraft:mg_tank>, [[<ore:plateBronze>, <ore:blockGlass>, <ore:plateBronze>], [<ore:blockGlass>, null, <ore:blockGlass>], [<ore:plateBronze>, <ore:blockGlass>, <ore:plateBronze>]]);
//tin can removal for pam harvest exploits
//mods.gregtech.PlateBender.removeRecipe(<IC2:itemTinCan>);


//rice
recipes.addShapeless(<harvestcraft:riceseedItem>, [<harvestcraft:riceItem>]);
//seawaeed
recipes.addShapeless(<harvestcraft:seaweedItem>, [<harvestcraft:seaweedseedItem>]);
//cranberry
recipes.addShapeless(<harvestcraft:cranberryItem>, [<harvestcraft:cranberryseedItem>]);

//bake the cake
recipes.remove(<harvestcraft:carrotcakeItem>);
recipes.remove(<harvestcraft:cheesecakeItem>);
recipes.remove(<harvestcraft:cherrycheesecakeItem>);
recipes.remove(<harvestcraft:pineappleupsidedowncakeItem>);
recipes.remove(<harvestcraft:chocolatesprinklecakeItem>);
recipes.remove(<harvestcraft:redvelvetcakeItem>);
recipes.remove(<harvestcraft:holidaycakeItem>);
recipes.remove(<harvestcraft:pumpkincheesecakeItem>);

//nexey: ofanix removal
recipes.remove(<Ztones:ofanix>);


//nexey coal coke torches
recipes.addShaped(<minecraft:torch>*8, [[<ImmersiveEngineering:material:6>], [<minecraft:stick>]]);

//nexey sand
//recipes.addShapeless(<minecraft:sand>,[<SGU:SGUSand>]);

//nexey aluminium IE plates
recipes.removeShapeless(<ImmersiveEngineering:metal:32>, [<ImmersiveEngineering:tool>, <ore:ingotAluminium>]);

//IE coke neexey
recipes.addShapeless(<Railcraft:fuel.coke>,[<ImmersiveEngineering:material:6>]);

recipes.remove(<ImmersiveEngineering:metalDevice2:5>);

// --- Reinforced Glass
//OutputArray, InputStack, InputFluid, OutputArrayChances, Time in Ticks, EnergyUsage
mods.gregtech.ChemicalBath.addRecipe([<IC2:blockAlloyGlass>], <IC2:itemPartAlloy>, <liquid:glass> * 432, [10000], 400, 4);

//pam salt
mods.gregtech.Assembler.addRecipe(<harvestcraft:saltItem>, <harvestcraft:potItem>*0, <gregtech:gt.integrated_circuit:3>*0, <liquid:water> * 1000, 160, 40);//  8 sec 8k
mods.gregtech.Assembler.addRecipe(<harvestcraft:saltItem>, <harvestcraft:potItem>*0, <gregtech:gt.integrated_circuit:2>*0, <liquid:water> * 1000, 800, 5); // 40 sec 4k EU
mods.gregtech.Assembler.addRecipe(<harvestcraft:saltItem>, <harvestcraft:potItem>*0, <gregtech:gt.integrated_circuit:1>*0, <liquid:water> * 1000, 3000, 1); // 150 sec 3k EU


// --- Diamond Block , zulf

Compressor.addRecipe(<minecraft:diamond_block>, <minecraft:diamond> * 9);
Compressor.addRecipe(<minecraft:diamond_block>, <IC2:itemPartIndustrialDiamond>* 9);

// --- Quartz Glass --- zulf
//OutputStack, InputMold, InputFluid, Time in Ticks, EnergyUsage
mods.gregtech.FluidSolidifier.addRecipe(<appliedenergistics2:tile.BlockQuartzGlass>, <gregtech:gt.metaitem.01:2516> * 1, <liquid:molten.glass> * 144, 200, 16);
mods.gregtech.FluidSolidifier.addRecipe(<appliedenergistics2:tile.BlockQuartzGlass>, <gregtech:gt.metaitem.01:2523> * 2, <liquid:molten.glass> * 144, 400, 16);
mods.gregtech.FluidSolidifier.addRecipe(<appliedenergistics2:tile.BlockQuartzGlass>, <gregtech:gt.metaitem.01:2522> * 4, <liquid:molten.glass> * 144, 600, 16);


// --- Rubber Boots ---
//OutputStack, InputMold, InputFluid, Time in Ticks, EnergyUsage
mods.gregtech.FluidSolidifier.addRecipe(<IC2:itemArmorRubBoots>, <minecraft:leather_boots> * 1, <liquid:molten.rubber> * 864, 200, 30);

// achievement get recipes - keda
recipes.addShapeless(<gregtech:gt.blockmachines:101>,[<gregtech:gt.blockmachines:101>]);
recipes.addShapeless(<gregtech:gt.blockmachines:100>,[<gregtech:gt.blockmachines:100>]);
recipes.addShapeless(<gregtech:gt.metaitem.01:23354>,[<gregtech:gt.metaitem.01:23354>]);
recipes.addShapeless(<gregtech:gt.blockmachines:1120>,[<gregtech:gt.blockmachines:1120>]);

// --- Reinforced Glass - zulf
//OutputArray, InputStack, InputFluid, OutputArrayChances, Time in Ticks, EnergyUsage
mods.gregtech.ChemicalBath.addRecipe([<IC2:blockAlloyGlass>], <IC2:itemPartAlloy>, <liquid:molten.glass> * 432, [10000], 400, 4);

//enderchest
//recipes.remove(<minecraft:ender_chest>);

//inserter
recipes.remove(<Magneticraft:inserter>);
recipes.addShapeless(<Magneticraft:inserter>,[<gregtech:gt.metaitem.01:32650>]);

//keda, rotarycraft
recipes.addShaped(<RotaryCraft:rotarycraft_item_shaftcraft:3>, [[<gregtech:gt.metaitem.01:17306>, <ore:plateStainlessSteel>, <gregtech:gt.metaitem.01:17306>], [<ore:ringStainlessSteel>, <ore:craftingToolWrench >, <ore:ringStainlessSteel>], [<RotaryCraft:rotarycraft_item_shaftcraft>, <RotaryCraft:rotarycraft_item_shaftcraft>, <RotaryCraft:rotarycraft_item_shaftcraft>]]);

//recipes.remove(<RotaryCraft:rotarycraft_item_engine>);
//recipes.addShaped(<RotaryCraft:rotarycraft_item_engine>, [[<RotaryCraft:rotarycraft_item_shaftcraft>, <ore:gearGtSmallStainlessSteel>, <RotaryCraft:rotarycraft_item_shaftcraft>], [<gregtech:gt.metaitem.01:17322>, <gregtech:gt.metaitem.01:32601>, <RotaryCraft:rotarycraft_item_shaftcraft>], [<RotaryCraft:rotarycraft_item_shaftcraft>, <RotaryCraft:rotarycraft_item_shaftcraft>, <RotaryCraft:rotarycraft_item_shaftcraft>]]);

recipes.remove(<RotaryCraft:rotarycraft_item_enginecraft:5>);
recipes.addShaped(<RotaryCraft:rotarycraft_item_enginecraft:5>, [[<ore:plateDoubleMagnalium>, <RotaryCraft:rotarycraft_item_shaftcraft>, <ore:plateDoubleMagnalium>], [<RotaryCraft:rotarycraft_item_shaftcraft>, <ore:craftingToolHardHammer>, <RotaryCraft:rotarycraft_item_shaftcraft>], [<ore:plateDoubleMagnalium>, <RotaryCraft:rotarycraft_item_shaftcraft>, <ore:plateDoubleMagnalium>]]);

recipes.remove(<RotaryCraft:rotarycraft_item_enginecraft:8>);
recipes.addShaped(<RotaryCraft:rotarycraft_item_enginecraft:8>, [[<ore:craftingToolHardHammer>, <ore:wireGt02Gold>, <gregtech:gt.metaitem.01:28306>], [<ore:wireGt02Gold>, <RotaryCraft:rotarycraft_item_borecraft:8>, <gregtech:gt.blockmachines:1421>], [<ore:ringStainlessSteel>, <ore:wireGt02Gold>, <ore:craftingToolSolderingIron>]]);

recipes.remove(<RotaryCraft:rotarycraft_item_enginecraft:15>);
recipes.addShaped(<RotaryCraft:rotarycraft_item_enginecraft:15>, [[<ore:wireGt02AnyCopper>, null, <ore:wireGt02AnyCopper>], [<RotaryCraft:rotarycraft_item_shaftcraft>, <ore:circuitGood>, <RotaryCraft:rotarycraft_item_shaftcraft>], [<RotaryCraft:rotarycraft_item_shaftcraft>, <ore:calclavia:ADVANCED_BATTERY>, <RotaryCraft:rotarycraft_item_shaftcraft>]]);

//recipes.remove(<RotaryCraft:rotarycraft_item_borecraft:4>);
//recipes.addShaped(<RotaryCraft:rotarycraft_item_borecraft:4>, [[<ore:circuitGood>]]);

recipes.remove(<RotaryCraft:rotarycraft_item_borecraft:9>);
recipes.addShaped(<RotaryCraft:rotarycraft_item_borecraft:9> * 4, [[<gregtech:gt.metaitem.01:32631>]]);

recipes.remove(<ImmersiveEngineering:metalDevice:11>);
recipes.addShaped(<ImmersiveEngineering:metalDevice:11> * 4, [[<gregtech:gt.metaitem.01:32630>]]);

recipes.remove(<RotaryCraft:rotarycraft_item_enginecraft:14>);
recipes.addShaped(<RotaryCraft:rotarycraft_item_enginecraft:14>, [[<gregtech:gt.metaitem.02:20306>, <ore:craftingToolHardHammer>, <ore:stickLongMagnalium>], [<ore:craftingToolFile>, <RotaryCraft:rotarycraft_item_enginecraft:5>, <ore:craftingToolWrench>], [<ore:stickLongMagnalium>, <ore:craftingToolSolderingIron>, <ore:gearGtSmallStainlessSteel>]]);

recipes.remove(<RotaryCraft:rotarycraft_item_enginecraft:16>);

recipes.addShaped(<RotaryCraft:rotarycraft_item_enginecraft:16>, [
	[<ore:craftingToolHardHammer>, <ore:craftingToolHardHammer>, <ore:stickLongMagnalium>], 
	[<ore:craftingToolWrench>, <ore:gearGtDiamond>, <ore:craftingToolFile>], 
	[<ore:stickLongMagnalium>, <ore:craftingToolSolderingIron>, null]]);


//paddlepanel
recipes.remove(<RotaryCraft:rotarycraft_item_enginecraft:13>);
recipes.addShaped(<RotaryCraft:rotarycraft_item_enginecraft:13>, [[<IC2:itemFluidCell>.withTag({Fluid:{FluidName:"hydrochloricacid_gt5u", Amount: 1000}}), <IC2:itemFluidCell>.withTag({Fluid:{FluidName:"hydrochloricacid_gt5u", Amount: 1000}}), <IC2:itemFluidCell>.withTag({Fluid:{FluidName:"hydrochloricacid_gt5u", Amount: 1000}})], [<gregtech:gt.metaitem.01:17313>, <TConstruct:heavyPlate:81>, <gregtech:gt.metaitem.01:17313>], [<RotaryCraft:rotarycraft_item_compacts:9>, <RotaryCraft:rotarycraft_item_compacts:9>, <RotaryCraft:rotarycraft_item_compacts:9>]]);

//nexey skystone
recipes.addShapeless(<appliedenergistics2:tile.BlockSkyStone>*2, [<Railcraft:brick.abyssal:*>, <Railcraft:brick.abyssal:*>]);
recipes.addShapeless(<appliedenergistics2:tile.BlockSkyStone>, [<Railcraft:cube:6>]);

// zulf recipe
Compressor.addRecipe(<ReactorCraft:reactorcraft_item_magnet>, <RotaryCraft:rotarycraft_item_modingots:65> * 2); Compressor.addRecipe(<ReactorCraft:reactorcraft_item_magnet>, <ReactorCraft:reactorcraft_item_raw:6> * 2); 

//<Zulfurlubak> // --- RotaryCraft Coal Coke --- 
recipes.addShaped(<RotaryCraft:rotarycraft_item_compacts:8>, [ [null, null, null], [null, <Railcraft:fuel.coke>, null], [null, null, null]]);

//keda pump machine casing
recipes.addShaped(<gregtech:gt.blockcasings2:10>, 
	[[<IC2:blockMachine>, <ore:craftingToolSolderingIron>]]);
//keda motor machine casing
recipes.addShaped(<gregtech:gt.blockcasings2:11>, 
	[[<ore:craftingToolSolderingIron>, <IC2:blockMachine> ]]);
